﻿using Newtonsoft.Json;

namespace OrbitalWitness.BackendTask.Core
{
    public class LeaseScheduleWrapperInput
    {

        [JsonProperty("leaseschedule")]
        public LeaseScheduleInput LeaseSchedule { get; set; }
    }

}
