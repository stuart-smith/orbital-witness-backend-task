﻿using Newtonsoft.Json;

namespace OrbitalWitness.BackendTask.Core
{
    public class ScheduleEntryInput
    {
        [JsonProperty("entryNumber")]
        public string EntryNumber { get; set; }

        [JsonProperty("entryDate")]
        public string EntryDate { get; set; }

        [JsonProperty("entryType")]
        public string EntryType { get; set; }

        [JsonProperty("entryText")]
        public IList<string> EntryText { get; set; }
    }
}
