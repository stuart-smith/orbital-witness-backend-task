﻿using Newtonsoft.Json;

namespace OrbitalWitness.BackendTask.Core
{
    public class LeaseScheduleInput
    {

        [JsonProperty("scheduleType")]
        public string ScheduleType { get; set; }

        [JsonProperty("scheduleEntry")]
        public IList<ScheduleEntryInput> ScheduleEntry { get; set; }
    }

}
