﻿using Newtonsoft.Json;

namespace OrbitalWitness.BackendTask.Core
{
    public class LeaseScheduleWrapperOutput
    {

        [JsonProperty("leaseschedule")]
        public LeaseScheduleOutput LeaseSchedule { get; set; }

        public LeaseScheduleWrapperOutput()
        {
            LeaseSchedule = new LeaseScheduleOutput();
        }
    }

}
