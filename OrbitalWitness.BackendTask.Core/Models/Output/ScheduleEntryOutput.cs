﻿namespace OrbitalWitness.BackendTask.Core
{
    public class ScheduleEntryOutput
    {
        public string RegistrationDateAndPlanRef { get; set; }

        public string PropertyDescription { get; set; }

        public string DateOfLeaseAndTerm { get; set; }

        public string LesseesTitle { get; set; }

        public List<string> Notes { get; set; }

        public string EntryNumber { get; set; }

        public string EntryDate { get; set; }
        public string EntryType { get; set; }

        public ScheduleEntryOutput()
        {
            Notes = new List<string>();
        }
    }
}
