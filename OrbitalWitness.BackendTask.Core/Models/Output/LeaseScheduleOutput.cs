﻿using Newtonsoft.Json;

namespace OrbitalWitness.BackendTask.Core
{
    public class LeaseScheduleOutput
    {

        [JsonProperty("scheduleType")]
        public string ScheduleType { get; set; }

        [JsonProperty("scheduleEntry")]
        public List<ScheduleEntryOutput> ScheduleEntry { get; set; }

        public LeaseScheduleOutput()
        {
            ScheduleEntry = new List<ScheduleEntryOutput>();
        }
    }

}
