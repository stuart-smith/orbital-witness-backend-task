﻿using Newtonsoft.Json;

namespace OrbitalWitness.BackendTask.Core.Services
{
    public interface IScheduleOfLeaseService
    {
        Task<IEnumerable<LeaseScheduleWrapperOutput>> ParseJson(string json);
    }

    public class ScheduleOfLeaseService : IScheduleOfLeaseService
    {
        private const int FULL_LENGTH = 73;
        private const int THREE_QUARTER_LENGTH = 57;
        private const int COLUMN_SIZE_2 = 30;
        private const int COLUMN_SIZE_3 = 16;
        private const int COLUMN_SIZE_4 = 11;

        public async Task<IEnumerable<LeaseScheduleWrapperOutput>> ParseJson(string json)
        {
            List<LeaseScheduleWrapperOutput> output = new List<LeaseScheduleWrapperOutput>();

            var leaseScheduleDataArray = JsonConvert.DeserializeObject<LeaseScheduleWrapperInput[]>(json);


            for (int leaseScheduleDataIndex = 0; leaseScheduleDataIndex < leaseScheduleDataArray.Length; leaseScheduleDataIndex++)
            {
                var leaseScheduleData = leaseScheduleDataArray[leaseScheduleDataIndex];
                var outputWrapper = new LeaseScheduleWrapperOutput
                {
                    LeaseSchedule = new LeaseScheduleOutput
                    {
                        ScheduleType = leaseScheduleData.LeaseSchedule.ScheduleType
                    }
                };


                for (int scheduleEntryIndex = 0; scheduleEntryIndex < leaseScheduleData.LeaseSchedule.ScheduleEntry.Count; scheduleEntryIndex++)
                {
                    var scheduleEntry = leaseScheduleData.LeaseSchedule.ScheduleEntry[scheduleEntryIndex];

                    var outputScheduleEntry = new ScheduleEntryOutput();
                    outputScheduleEntry.EntryType = scheduleEntry.EntryType;
                    outputScheduleEntry.EntryNumber = scheduleEntry.EntryNumber;
                    outputScheduleEntry.EntryDate = scheduleEntry.EntryDate;

                    for (int scheduleEntryTextIndex = 0; scheduleEntryTextIndex < scheduleEntry.EntryText.Count; scheduleEntryTextIndex++)
                    {
                        var scheduleEntryText = scheduleEntry.EntryText[scheduleEntryTextIndex];

                        // if its empty dont try to do anything with it
                        if (string.IsNullOrWhiteSpace(scheduleEntryText)) continue;

                        // some checks
                        var isNote = scheduleEntryText?.StartsWith("NOTE") == true;
                        var isFullLength = scheduleEntryText.Length == FULL_LENGTH;

                        //if its cancelled, add it as a note
                        if (scheduleEntryTextIndex == 0 && scheduleEntryText.Contains("CANCELLED"))
                        {
                            outputScheduleEntry.Notes.Add(scheduleEntryText);
                        }

                        //if its the first row and its 57 chars in length is probably missing first column
                        if (scheduleEntryTextIndex == 0 && scheduleEntryText.Length == THREE_QUARTER_LENGTH)
                        {
                            outputScheduleEntry.PropertyDescription += scheduleEntryText.Substring(0, 30).Trim();
                            outputScheduleEntry.DateOfLeaseAndTerm += scheduleEntryText.Substring(30, 16).Trim();
                            outputScheduleEntry.LesseesTitle += scheduleEntryText.Substring(30 + 16, 11).Trim();
                            continue;
                        }

                        //if its the first row and its 53 chars in length but ends with whitespace is probably missing two columns
                        if (scheduleEntryTextIndex == 0 && scheduleEntryText.Length == 53 && scheduleEntryText.EndsWith(' '))
                        {
                            outputScheduleEntry.DateOfLeaseAndTerm += scheduleEntryText.Substring(30, 16).Trim();
                            outputScheduleEntry.LesseesTitle += scheduleEntryText.Substring(30 + 16, 11).Trim();
                            continue;
                        }

                        //if its first row and its 57 chars meaning full length
                        if (scheduleEntryTextIndex == 0 && isFullLength)
                        {
                            outputScheduleEntry.RegistrationDateAndPlanRef = scheduleEntryText.Substring(0, 10).Trim();
                            outputScheduleEntry.PropertyDescription = scheduleEntryText.Substring(16, COLUMN_SIZE_2).Trim();
                            outputScheduleEntry.DateOfLeaseAndTerm = scheduleEntryText.Substring(16 + COLUMN_SIZE_2, COLUMN_SIZE_3).Trim();
                            outputScheduleEntry.LesseesTitle = scheduleEntryText.Substring(16 + COLUMN_SIZE_2 + COLUMN_SIZE_3, COLUMN_SIZE_4).Trim();
                            continue;
                        }

                        //if its not a note and not the first row and 73 characters in length
                        if (!isNote && scheduleEntryTextIndex > 0 && scheduleEntryText?.Length == 73)
                        {
                            outputScheduleEntry.RegistrationDateAndPlanRef += $" {scheduleEntryText.Substring(0, 10).Trim()}".TrimEnd();
                            outputScheduleEntry.PropertyDescription += $" {scheduleEntryText.Substring(16, COLUMN_SIZE_2).Trim()}".TrimEnd();
                            outputScheduleEntry.DateOfLeaseAndTerm += $" {scheduleEntryText.Substring(16 + COLUMN_SIZE_2, COLUMN_SIZE_3).Trim()}".TrimEnd();
                            outputScheduleEntry.LesseesTitle += $" {scheduleEntryText.Substring(16 + COLUMN_SIZE_2 + COLUMN_SIZE_3, COLUMN_SIZE_4).Trim()}".TrimEnd();
                            continue;
                        }

                        if (!isNote && scheduleEntryText?.Length > 73)
                        {
                            if (char.IsNumber(scheduleEntryText[0]))
                            {
                                outputScheduleEntry.RegistrationDateAndPlanRef += $" {scheduleEntryText.Substring(0, 10).Trim()}";
                                outputScheduleEntry.PropertyDescription += $" {scheduleEntryText.Substring(16, COLUMN_SIZE_2 + 1).Trim()}";
                                outputScheduleEntry.DateOfLeaseAndTerm += $" {scheduleEntryText.Substring(16 + COLUMN_SIZE_2, COLUMN_SIZE_3 + 1).Trim()}";
                                outputScheduleEntry.LesseesTitle += $" {scheduleEntryText.Substring(16 + COLUMN_SIZE_2 + COLUMN_SIZE_3, COLUMN_SIZE_4).Trim()}";
                            }
                            else
                            {
                                outputScheduleEntry.Notes[outputScheduleEntry.Notes.Count - 1] = outputScheduleEntry.Notes[outputScheduleEntry.Notes.Count - 1] += scheduleEntryText;
                            }
                            continue;
                        }

                        if (!isNote && scheduleEntryTextIndex > 0 && scheduleEntryText?.Length < 73)
                        {
                            if (scheduleEntryText?.Length < 16)
                            {
                                outputScheduleEntry.RegistrationDateAndPlanRef += $" {scheduleEntryText}";
                                continue;
                            }

                            if (scheduleEntryText?.Length <= 27 && scheduleEntryText.Length >= 16)
                            {
                                outputScheduleEntry.DateOfLeaseAndTerm += $" {scheduleEntryText.Substring(0, 16).Trim()}";
                                continue;
                            }

                            if (scheduleEntryText?.Length == 55)
                            {
                                outputScheduleEntry.RegistrationDateAndPlanRef += $" {scheduleEntryText.Substring(0, 16).Trim()}";
                                outputScheduleEntry.DateOfLeaseAndTerm += $" {scheduleEntryText.Substring(scheduleEntryText.Length - 10, 10).Trim()}";
                                continue;
                            }
                        }

                        if (isNote)
                        {
                            outputScheduleEntry.Notes.Add(scheduleEntryText);
                        }

                    }

                    outputWrapper.LeaseSchedule.ScheduleEntry.Add(outputScheduleEntry);
                }

                output.Add(outputWrapper);
            }


            return output;
        }
    }
}
