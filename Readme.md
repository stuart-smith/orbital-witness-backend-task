# Orbital Witness Backend Task

## Overview

This solution demonstrates how to parse the provided JSON file (schedule_of_notices_of_lease_examples.json) into a more structured format.

Disclaimer: The current solution is not considered to work perfectly, but is provided to show a half working solution with dependency injection/basic tests (since it doesnt work)/user interface/reusability etc...

The solution contains 4 projects:

- OrbitalWitness.BackendTask - .NET 6.0 Console App for testing the JSON parsing
- OrbitalWitness.BackendTask.Core - .NET 6.0 Class Library with models and services that are used by web app and console app
- OrbitalWitness.BackendTask.WebApp - .NET 6.0/ASP .NET Core Web project with a Razor UI for uploading a JSON file and seeing the parsed results
- OrbitalWitness.BackendTask.Tests - .NET 6.0/MSTest project with some basic unit test

## System Requirements

You must have the following tools installed to run the solution

- .NET 6.0 SDK

## How to run the console app

You can run the console app by opening by running RunConsole.ps1 in the root folder of the repository

RunConsole.ps1 will perform the following tasks:

- Release build of OrbitalWitness.BackendTask project
- Run the OrbitalWitness.BackendTask application
- Output will saved at the displayed path

## How to run the web app

You can run the web app by opening by running RunWeb.ps1 in the root folder of the repository

RunWeb.ps1 will perform the following tasks:

- Release build of OrbitalWitness.BackendTask.WebApp project
- Run the OrbitalWitness.BackendTask.WebApp application
- Starts a browser to https://localhost:7055/


## Notes

- Console app saves output in the Output folder where the OribitalWitness.BackendTask.exe file is located
- Web App is running at https://localhost:7055/