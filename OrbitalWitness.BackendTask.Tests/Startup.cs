﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OrbitalWitness.BackendTask.Core.Services;

namespace OrbitalWitness.BackendTask.Tests
{
    public static class Startup
    {
        public static IHost HostApp { get; private set; }

        public static void Initialize()
        {
            HostApp = Host.CreateDefaultBuilder()
                .ConfigureServices((services) =>
                {
                    services.AddTransient<IScheduleOfLeaseService, ScheduleOfLeaseService>();
                })
                .Build();
        }
    }
}