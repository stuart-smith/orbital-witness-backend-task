using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using OrbitalWitness.BackendTask.Core;
using OrbitalWitness.BackendTask.Core.Services;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OrbitalWitness.BackendTask.Tests
{
    [TestClass]
    public class UnitTests
    {
        public UnitTests()
        {
            Startup.Initialize();
            ScheduleOfLeaseService = Startup.HostApp.Services.GetService<IScheduleOfLeaseService>();
        }

        public IScheduleOfLeaseService? ScheduleOfLeaseService { get; }

        [TestMethod]
        public async Task CanParseJson()
        {
            var originalJson = await File.ReadAllTextAsync("schedule_of_notices_of_lease_examples.json");
            Assert.IsFalse(string.IsNullOrWhiteSpace(originalJson));
            var originalData = JsonConvert.DeserializeObject<LeaseScheduleWrapperInput[]>(originalJson);

            var fixedData = await ScheduleOfLeaseService.ParseJson(originalJson);

            Assert.AreEqual(originalData.Count(), fixedData.Count());
            Assert.AreEqual(originalData.Select(e=>e.LeaseSchedule).SelectMany(e=>e.ScheduleEntry).Count(),
                fixedData.Select(e => e.LeaseSchedule).SelectMany(e => e.ScheduleEntry).Count());
        }
    }
}