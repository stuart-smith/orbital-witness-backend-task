﻿using Microsoft.AspNetCore.Mvc;
using OrbitalWitness.BackendTask.Core.Services;
using System.Text;

namespace OrbitalWitness.BackendTask.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public IScheduleOfLeaseService ScheduleOfLeaseService { get; }

        public HomeController(IScheduleOfLeaseService scheduleOfLeaseService)
        {
            ScheduleOfLeaseService = scheduleOfLeaseService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile jsonFile)
        {
            if (jsonFile != null)
            {
                var json = new StringBuilder();
                using (var reader = new StreamReader(jsonFile.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        json.AppendLine(await reader.ReadLineAsync());
                }

                var results = await ScheduleOfLeaseService.ParseJson(json.ToString());
                return View(results);

            }

            return View();
        }
    }
}