﻿using Newtonsoft.Json;
using OrbitalWitness.BackendTask.Core.Services;
using System;
using System.IO;
using System.Threading.Tasks;

namespace OrbitalWitness.BackendTask
{
    class Program
    {
        const string DATA_FILENAME = "schedule_of_notices_of_lease_examples.json";
        const string DATA_DIR = "Data";        

        static async Task Main(string[] args)
        {
            var json = await File.ReadAllTextAsync(Path.Combine(DATA_DIR, DATA_FILENAME));
            var scheduleOfLeaseService = new ScheduleOfLeaseService();
            var output = await scheduleOfLeaseService.ParseJson(json);
            var outputJson = JsonConvert.SerializeObject(output, Formatting.Indented);
            var outputPath = Path.Combine("Output", $"Output_{DateTime.Now.ToString("yyyyMMddTHHmmss")}.json");
            Directory.CreateDirectory("Output");
            await File.WriteAllTextAsync(outputPath, outputJson);
            Console.WriteLine(outputJson);
            Console.WriteLine();
            Console.WriteLine("========================================");
            Console.WriteLine($"Output has been saved to {outputPath}");
            Console.WriteLine("========================================");
            Console.WriteLine();
        }
    }
}